###################################################################################
# LAVA QA tool
# Copyright (C) 2015  Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

from __future__ import print_function

from lqa_api.job import Job, JobError
from lqa_api.exit_codes import APPLICATION_ERROR
from lqa_tool.commands import Command
from lqa_tool.settings import lqa_logger

class StatusCmd(Command):

    def __init__(self, args):
        Command.__init__(self, args)

    def run(self):
        for job_id in self.job_ids:
            try:
                job = Job(job_id, self.server)
                print("{}: {}".format(job.status, job))
            except JobError as err:
                lqa_logger.error("status command: {}".format(err))
                exit(APPLICATION_ERROR)
